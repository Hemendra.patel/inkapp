"use strict";
import config from "./../config/app-config.json";

const _fs = require("fs");
const _ssh = require("ssh2");
const _path = require("path");
const _crypto = require("crypto");
const sshPort = config.ssh_port;
const _rfScreen = require("../screen.component");

const allowedPublicKey = _ssh.utils.parseKey(
	_fs.readFileSync(_path.join(__dirname, "../../ssh/user.pub"))
);
const hostKeys = [
	_fs.readFileSync(_path.join(__dirname, "../../ssh/host_key")),
];

let rfScreen = {};
function noop() {}

const sshServer = _ssh.Server(
	{
		hostKeys,
	},
	(client: any) => {
		let stream: any = null;
		let session = null;
		client.remoteAddress = client._sock.remoteAddress || "";
		client.on("authentication", (ctx: any) => {
			// if (!(ctx.username || ctx.user)) {
			// 	return ctx.reject();
			// }
			// client.terminal = (ctx.username || ctx.user).split("/")[0];
			// if (ctx.method !== "publickey") {
			// 	return ctx.reject(["publickey"]);
			// }

			// const allowedPubSSHKey = allowedPublicKey.getPublicSSH();

			// if (
			// 	ctx.key.algo !== allowedPublicKey.type ||
			// 	ctx.key.data.length !== allowedPubSSHKey.length ||
			// 	!_crypto.timingSafeEqual(ctx.key.data, allowedPubSSHKey) ||
			// 	(ctx.signature &&
			// 		allowedPublicKey.verify(ctx.blob, ctx.signature) !== true)
			// ) {
			// 	return ctx.reject();
			// }
			return ctx.accept();
		});
		client.on("ready", () => {
			client.once("session", (accept: any) => {
				session = accept();
				session
					.once("pty", (accept: any, reject: any, info: any) => {
						client.rows = info.rows;
						client.cols = info.cols;
						// stream = accept();
					})
					.once("shell", (accept: any) => {
						stream = accept();
						stream.rows = client.rows || 20;
						stream.columns = client.cols || 30;
						stream.isTTY = true;
						stream.terminal = client.terminal;
						stream.setRawMode = noop;
						_rfScreen.buildscreen(stream);
					});
			});
		});
	}
);

sshServer.listen(sshPort, () => {
	console.log(`RF SSH server listening on port ${sshPort}`);
});
