import React, { FC, useEffect } from "react";
import { Box, useFocusManager } from "ink";
import { useHistory } from "react-router";
import WmsInput from "./wms-input";
// import axios from "axios";
const App: FC<{}> = () => {
	const history = useHistory();
	const { focusNext } = useFocusManager();

	useEffect(() => {
		focusNext();
		// axios({
		// 	method: "get",
		// 	url: "https://restcountries.eu/rest/v2/callingcode/372",
		// 	responseType: "stream",
		// }).then((response: any) => {
		// 	console.log("response.responseUrl");
		// 	console.log(response.responseUrl);
		// });
	}, []);

	// history.push('/dashboard')
	const handleSubmit = () => {
		history.push("/dashboard");
		// Do something with query
	};

	return (
		<Box flexDirection="column">
			<Box>
				<WmsInput onSubmit={handleSubmit}></WmsInput>
			</Box>
			<Box marginTop={1}>
				<WmsInput onSubmit={handleSubmit}></WmsInput>
			</Box>
		</Box>
	);
};

module.exports = App;
export default App;
