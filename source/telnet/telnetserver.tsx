const _telnet = require("telnet2");
const rfScreen = require("../screen.component");
const telnetPort = "2002";

const telnetServer = _telnet(
	{
		tty: true,
	},
	(client: any) => {
		console.log("start");
		client.columns = 30;
		client.rows = 20;
		client.setEncoding = () => {};
		client.once("term", () => {
			try {
				rfScreen.buildscreen(client);
			} catch (error) {
				throw error;
			}
		});
		client.on("end", function () {
			client.end();
		});
		client.once("close", () => {
			client.destroy();
			client = undefined;
		});
	}
);

telnetServer.listen(telnetPort, () => {
	console.log("Telnet server listening on port " + telnetPort);
});
