#!/usr/bin/env node
import React from "react";
import App from "./app";
import { Router, Switch, Route } from "react-router";
import { createMemoryHistory } from "history";
import { render, RenderOptions, Box } from "ink";
import Dashboard from "./dashboard";
const history = createMemoryHistory();

export function buildscreen(client: any) {
	const renderoption: RenderOptions = {
		stdout: client,
		stdin: client,
		debug: false,
	};

	render(
		<Box
			flexDirection="column"
			borderColor="red"
			borderStyle="single"
			height={20}
			width={30}
		>
			<Router history={history}>
				<Switch>
					<Route path="/dashboard" exact>
						<Dashboard />
					</Route>
					<Route path="/" exact>
						<App />
					</Route>
				</Switch>
			</Router>
		</Box>,
		renderoption
	);
	// render(<LayoutComponent></LayoutComponent>);
}
