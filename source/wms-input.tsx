import React, { useState } from "react";
import TextInput from "ink-text-input";
import { useFocus, Box, Text } from "ink";

const WmsInput = ({ onSubmit }: { onSubmit: any }) => {
	const [query, setQuery] = useState("");

	const { isFocused } = useFocus();

	return (
		<Box>
			<Box marginRight={1}>
				<Text>Enter your query:</Text>
			</Box>

			<TextInput
				value={query}
				onChange={setQuery}
				focus={isFocused}
				onSubmit={onSubmit}
			/>
		</Box>
	);
};

export default WmsInput;
