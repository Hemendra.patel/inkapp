#!/usr/bin/env node
import React from "react";
// import { render } from "ink";
import App from "./app";
import { Router, Switch, Route } from "react-router";
import { createMemoryHistory } from "history";
// import blessed from 'blessed';
import { render } from "ink";
import Dashboard from "./dashboard";
const history = createMemoryHistory();
// const screen = new NodeJS.WriteStream();
// Rendering the React app using our screen
render(
	<Router history={history}>
		<Switch>
			<Route path="/dashboard" exact>
				<Dashboard />
			</Route>
			<Route path="/" exact>
				<App />
			</Route>
		</Switch>
	</Router>
);
