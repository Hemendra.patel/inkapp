import React from "react";
import TextInput from "ink-text-input";

class Item_Component extends React.Component<any, any> {
	constructor(props: any) {
		super(props);
		this.state = { name: "" };
	}
	public handleOnChange = (event: any) => {
		this.setState({ name: event });
	};
	render() {
		return <TextInput value={this.state.name} onChange={this.handleOnChange} />;
	}
}

export default Item_Component;
