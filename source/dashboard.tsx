import React, { FC, useEffect } from "react";
import { Box, useFocusManager } from "ink";
import { useHistory } from "react-router";
import WmsSelect from "./wms-select";
import WmsInput from "./wms-input";

const Dashboard: FC<{}> = () => {
	const history = useHistory();
	const { focusNext } = useFocusManager();

	useEffect(() => {
		focusNext();
	}, []);
	// history.push('/dashboard')
	const handleSubmit = () => {
		history.push("/");
		// Do something with query
	};

	return (
		<Box flexDirection="column">
			<WmsSelect onSelect={handleSubmit} />
			<WmsInput onSubmit={handleSubmit}></WmsInput>
		</Box>
	);
};

module.exports = Dashboard;
export default Dashboard;
