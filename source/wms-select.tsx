import React from "react";
import { Box, Text, useFocus } from "ink";
import SelectInput from "ink-select-input";

const WmsSelect = ({ onSelect }: { onSelect: () => void }) => {
	const items = [
		{
			label: "First",
			value: "first",
		},
		{
			label: "Second",
			value: "second",
		},
		{
			label: "Third",
			value: "third",
		},
	];
	const { isFocused } = useFocus();
	return (
		<Box flexDirection="row">
			<Box marginRight={1}>
				<Text>Please select a value:</Text>
			</Box>

			<Box marginTop={3}>
				<SelectInput items={items} onSelect={onSelect} isFocused={isFocused} />
			</Box>
		</Box>
	);
};

export default WmsSelect;
